# Define required providers
terraform {
  required_version = ">= 1.3.0"
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "~> 1.49"
    }
    vkcs = {
      source = "vk-cs/vkcs"
      version = "~> 0.1.12"
   }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  user_name         = var.openstack_conf.user_name       
  tenant_id         = var.openstack_conf.tenant_id     
  auth_url          = var.openstack_conf.auth_url        
  region            = var.openstack_conf.region          
  user_domain_name  = var.openstack_conf.user_domain_name
  password          = var.openstack_conf.password
  
}

# Configure the VKCS Provider
provider "vkcs" {
  username    = var.openstack_conf.user_name
  project_id  = var.openstack_conf.tenant_id
  auth_url    = var.openstack_conf.auth_url
  region      = var.openstack_conf.region
  password    = var.openstack_conf.password
}
