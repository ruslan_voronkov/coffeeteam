# Create internal netwoks and subnets
# Create routers and add internal subnets to routers

# Create networks
resource "vkcs_networking_network" "networks" {
  
  for_each = var.networks
  name                  = each.key
  port_security_enabled = true
//  shared                = "false"
  admin_state_up        = true
}

locals {

# Flatten networks to subnets
raw_subnets = flatten([
    for network_key, network in var.networks : [
      for subnet_key, subnet in network : {
        network_key   = network_key
        network_id    = vkcs_networking_network.networks[network_key].id
        subnet-name   = subnet_key
        router-name   = subnet.router-name
        cidr          = subnet.cidr
        start         = subnet.start
        end           = subnet.end
        gw_ip         = subnet.gw_ip
        dhcp          = subnet.dhcp
      }
    ]
  ])

# Subnets mapping  
  subnets = {for subnet in local.raw_subnets : "${subnet.network_key}.${subnet.subnet-name}" => subnet}

# Map routers to subnets
routers = {for router in distinct([
              for subnet in local.subnets : subnet.router-name]) :
                router => [for k, subnet_set in local.subnets :
                  k if subnet_set.router-name == router ] }

# Flatten port from instances
ports = {for map_port in flatten([
            for instance_name, instance in var.instances : [ 
              for port_name, port in instance.ports : {
                instance-name = instance_name
                port-name     = port_name
                net-name      = port.net-name
                subnet-name   = port.subnet-name
                ip            = port.ip
                sec_groups    = port.sec_groups
                dns_name      = port.dns_name
            }]]) : "${map_port.instance-name}.${map_port.port-name}" => map_port}

# Map rules to security groups
net_rules = {for map_rule in flatten([
                for group_key, group_value in var.sec_groups : [
                  for rule in group_value.rules : {
                    direction         = var.rules[rule].direction
                    protocol          = var.rules[rule].protocol
                    port_min    = var.rules[rule].port_min
                    port_max    = var.rules[rule].port_max
                    remote_ip_prefix  = var.rules[rule].remote_ip_prefix
                    remote_group       = var.rules[rule].remote_group 
                    group_key = group_key
                    rule_key =  rule    
                  }]]) : "${map_rule.group_key}.${map_rule.rule_key}" => map_rule }

# Get all security groups name from instances ports
  all_sec_groups = toset(flatten([for port in local.ports : port.sec_groups[*] ]))

# Map db to dbms instances
databases = {for db in flatten([for dbms_name, dbms_value in var.clusters : [
                for db_name, db_value in dbms_value.db : {
                  dbms_name = dbms_name
                  db_name   = db_name
                  charset   = db_value.charset
                  collate   = db_value.collate
                }]]) : "${db.dbms_name}.${db.db_name}" => db}

# Map listeners to balancers
listeners = {for listener in flatten([for lb_name, lb_value in var.loadbalancers :
              [for lsn_name, lsn_value in lb_value.listeners : {
                lb_name        = lb_name
                name           = lsn_name
                protocol       = lsn_value.protocol
                protocol_port  = lsn_value.protocol_port
                default_tls_container_name = lsn_value.default_tls_container_name
                insert_headers = lsn_value.insert_headers
              }]]) : "${listener.lb_name}.${listener.name}" => listener}
}


# Create subnets
resource "openstack_networking_subnet_v2" "subnets" {
  
  for_each = local.subnets
  
  name          = each.value.subnet-name
  network_id    = each.value.network_id
  cidr          = each.value.cidr
  allocation_pool {
    start = each.value.start
    end   = each.value.end
  }
  enable_dhcp = each.value.dhcp
  gateway_ip  = each.value.gw_ip
  ip_version  = 4
}


# Get external terraform resource
data "vkcs_networking_network" "ext-network" {
  name = "ext-net"
}


# Create routers with external IP from "ext-net" network
resource "openstack_networking_router_v2" "routers" {

  for_each = local.routers

  name                = each.key
  admin_state_up      = true
  external_network_id = data.vkcs_networking_network.ext-network.id
}


# Add internal subnets to routers
resource "openstack_networking_router_interface_v2" "router-ifs" {

  for_each = local.subnets

  router_id = openstack_networking_router_v2.routers[each.value.router-name].id
  subnet_id = openstack_networking_subnet_v2.subnets[each.key].id

  depends_on = [
    openstack_networking_router_v2.routers,
    openstack_networking_subnet_v2.subnets
  ]
}


# Create security groups 
resource "vkcs_networking_secgroup" "secgroups" {
  
  for_each = var.sec_groups

  name        = each.key
  description = each.value.description
  delete_default_rules = each.value.delete_default_rules

}


# Create rules and add to security groups
resource "vkcs_networking_secgroup_rule" "secgroup_rules" {
  
  for_each = local.net_rules
  
//  ethertype         = "IPv4"
  direction         = each.value.direction
  protocol          = each.value.protocol
  port_range_min    = each.value.port_min
  port_range_max    = each.value.port_max
  remote_ip_prefix  = each.value.remote_ip_prefix == null ? null : each.value.remote_ip_prefix 
  security_group_id = vkcs_networking_secgroup.secgroups[each.value.group_key].id
  remote_group_id   = each.value.remote_group == null ? null : vkcs_networking_secgroup.secgroups[each.value.remote_group].id
  
  depends_on = [
    vkcs_networking_secgroup.secgroups
  ]
}


# Create ports to provide access between subnets
resource "openstack_networking_port_v2" "ports" {
  
  for_each = local.ports

###   name               = "${each.value.instance-name}.${each.value.port-name}"
  description           = each.value.dns_name
###   dns_name           = each.value.dns_name
  network_id            = vkcs_networking_network.networks[each.value.net-name].id
  # Security groups will be added after instaces creation due to bug 
  # - error while creating instances with ports with applied sucurity groups
  security_group_ids    =  []  
  admin_state_up        = true
  port_security_enabled = true

  depends_on = [
    vkcs_networking_network.networks,
    openstack_networking_subnet_v2.subnets
  ]
}

# Create netplan YAML config for instances
# Pay attention!!! Indent sensetive!!!
locals {
  netplan_config = {for instance_key, instance_value in var.instances :
instance_key => base64encode(<<EOT
    %{~for port_key, port_value in instance_value.ports~}
    ${port_key}:
      match:
        macaddress: ${openstack_networking_port_v2.ports["${instance_key}.${port_key}"].mac_address}
      set-name: ${port_key} 
      dhcp4: true
      dhcp4-overrides:
        route-metric: %{if port_value.default_if}100%{ else }200%{ endif}
    %{~endfor~}
EOT
)}
}



# Create cloud-init parts for all instances using templates
data "template_file" "instances-cloud-init" {
 
 for_each = {for cfg in flatten([for instance_key, instance_value in var.instances : [
              for cfg_key, cfg_value in instance_value.cloud_init : {
                instance_key = instance_key
                cfg_key      = cfg_key
                cfg_path     = cfg_value
              }]]) : "${cfg.instance_key}.${cfg.cfg_key}" => cfg}

 template = "${file("${path.module}/${each.value.cfg_path}")}"
 vars = {
   config_body = local.netplan_config[each.value.instance_key]
   rsa_private = openstack_compute_keypair_v2.adm-keypair.private_key
   rsa_public  = openstack_compute_keypair_v2.adm-keypair.public_key
 }

 depends_on = [
    openstack_compute_keypair_v2.adm-keypair
 ]
}


# Create final cloud-init per instance from parts above
data "template_cloudinit_config" "instances-configs" {

  for_each = var.instances

  gzip          = true
  base64_encode = true

  dynamic "part" {
    for_each = each.value.cloud_init
    content {
      filename     = "${each.key}.${part.key}"
      content = data.template_file.instances-cloud-init["${each.key}.${part.key}"].rendered
    }
  }

  depends_on = [
    data.template_file.instances-cloud-init
  ]
}



### resource "local_file" "debug_adm_tempate" {
###     content  = data.template_cloudinit_config.instances-configs["adm-node1-ms1"].rendered
###     filename = "${path.module}/adm-debug.cfg"
### }
### 
### output "debug_cloud_init" {
###   value = data.template_cloudinit_config.instances-configs["adm-node1-ms1"].rendered
### }


###output "Compiled-cloud-init" {
###  value = base64decode(data.template_cloudinit_config.instances-configs["adm-node1-ms1"].rendered)
###}



# Create instances
resource "vkcs_compute_instance" "instances" {

  for_each = var.instances

  name              = each.key
  flavor_name       = each.value.flavor_name
  security_groups   = each.value.sec_groups
  key_pair          = each.value.key_pair != "add-adm-key" ? each.value.key_pair : var.adm_key_name
  availability_zone = each.value.az 
  user_data         = each.value.cloud_init == null ? null : data.template_cloudinit_config.instances-configs[each.key].rendered

  block_device {
    uuid                  = each.value.image
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = each.value.vol_type
    volume_size           = each.value.vol_size
    boot_index            = 0
    delete_on_termination = each.value.delete
  }


  dynamic "network" {
    for_each = each.value.ports
    content {
      port = openstack_networking_port_v2.ports["${each.key}.${network.key}"].id
    }
  }

  depends_on = [
    vkcs_networking_network.networks,
    openstack_networking_subnet_v2.subnets,
    openstack_compute_keypair_v2.adm-keypair,
    openstack_networking_router_interface_v2.router-ifs,
    data.template_cloudinit_config.instances-configs
  ]
}


# Associate security groups to instance's ports
resource "openstack_networking_port_secgroup_associate_v2" "rules-to-ports" {

  for_each = local.ports

  port_id = openstack_networking_port_v2.ports[each.key].id
  security_group_ids = [for group_name in each.value.sec_groups : 
                          vkcs_networking_secgroup.secgroups[group_name].id ]

  enforce            = "true"

  depends_on = [
    vkcs_compute_instance.instances,
    vkcs_networking_secgroup.secgroups,
    vkcs_networking_secgroup_rule.secgroup_rules,
    openstack_networking_port_v2.ports
  ]
}


# Create cloud auth for openstack
resource "local_file" "clouds" {
    content  = jsonencode({"clouds" = {
                            "${var.openstack_conf.clouds_cfg_name}" = {
                              "region_name" = var.openstack_conf.region,
                              "auth" = {
                                "username"   = var.openstack_conf.user_name,
                                "password"   = var.openstack_conf.password,
                                "auth_url"   = var.openstack_conf.auth_url,
                                "project_id" = var.openstack_conf.tenant_id,
                                "user_domain_name" = var.openstack_conf.user_domain_name},
                              "identity_api_version" = "3",
                              "interface" = "public"}
                          }})
    filename = "${path.module}/clouds.yaml"

    lifecycle {
      create_before_destroy = false
    }

}


# Create ports json to setup DNS for each port
locals {
  ports_attrs = base64encode(jsonencode([for port_key, port_value in local.ports : {
                     port_key  =  port_key,
                     port_id   =  openstack_networking_port_v2.ports[port_key].id,
                     dns_name  =  port_value.dns_name
                }]))
}


# Ports setup - set DNS for each port
resource "null_resource" "run_port_setup" {
  
  provisioner "local-exec" {
    command = "python3 port-dns-setup.py ${var.openstack_conf.clouds_cfg_name} ${local.ports_attrs}"
  }

  depends_on = [
    openstack_networking_port_secgroup_associate_v2.rules-to-ports,
    local.ports_attrs,
    local_file.clouds
  ]

}


# Provide external IPs for instances from "ext-net" network
resource "openstack_networking_floatingip_v2" "instances-ext-ip" {

  for_each = toset(flatten([for instance_key, instance_value in var.instances : [
                      for port_key, port_value in instance_value.ports :
                        "${instance_key}.${port_key}" if port_value.ext_ip ]]))
  pool = "ext-net"

  depends_on = [
    vkcs_compute_instance.instances
  ]

  lifecycle {
    prevent_destroy = false
  }
}


# Associate external IP with instances
resource "openstack_networking_floatingip_associate_v2" "instances-ext-ip" {
  
  for_each = toset(flatten([for instance_key, instance_value in var.instances : [
                      for port_key, port_value in instance_value.ports :
                        "${instance_key}.${port_key}" if port_value.ext_ip ]]))
  
  floating_ip = openstack_networking_floatingip_v2.instances-ext-ip[each.value].address
  port_id = openstack_networking_port_v2.ports[each.value].id

  depends_on = [
    openstack_networking_floatingip_v2.instances-ext-ip,
    vkcs_compute_instance.instances
  ]
}


# Create DB cluster
resource "vkcs_db_cluster" "clusters" {

  for_each = var.clusters

  name        = each.key

  datastore {
    type    = each.value.type
    version = each.value.version
  }

  cluster_size = each.value.cluster_size
  flavor_id   = each.value.flavor_id
  volume_size = each.value.volume_size
  volume_type = each.value.volume_type
  keypair     = each.value.key_pair != "add-adm-key" ? each.value.key_pair : var.adm_key_name
  availability_zone =  each.value.availability_zone

  network {
    uuid = vkcs_networking_network.networks[each.value.network].id
  }

  dynamic "capabilities" {
    
    for_each = each.value.capabilities

    content {
      name = capabilities.key
      settings = capabilities.value.settings
    }
  }
  
  depends_on = [
    vkcs_networking_network.networks,
    openstack_networking_subnet_v2.subnets,
    vkcs_networking_secgroup.secgroups,
    openstack_compute_keypair_v2.adm-keypair,
    openstack_networking_router_interface_v2.router-ifs
 //   openstack_networking_port_secgroup_associate_v2.rules-to-ports
  ]

}


# Craete cluster json to execute cluster-setup
locals {
  clusters_attrs = base64encode(jsonencode([for cluster_key, cluster_value in var.clusters : {
                     cluster_name    =  cluster_key,
                     cluster_id      =  vkcs_db_cluster.clusters[cluster_key].id,
                     dns_name        =  cluster_value.dns_name,
                     network         =  {"name": cluster_value.network,
                                         "id": vkcs_networking_network.networks[cluster_value.network].id},
                     loadbalancer    =  {"id": "", "vip_port_id": ""},
                     pool_instances  = [],
                     security_groups = [for sec_group in cluster_value.sec_groups : {
                                          "id": vkcs_networking_secgroup.secgroups[sec_group].id,
                                          "name": sec_group}]
                      }]))
}

# Cluster setup - security groups and DNS for cluster loadbalancer
resource "null_resource" "run_cluster_setup" {
  
  provisioner "local-exec" {
    command = "python3 cluster-setup.py ${var.openstack_conf.clouds_cfg_name} ${local.clusters_attrs}"
  }

  depends_on = [
    vkcs_db_cluster.clusters,
    local.clusters_attrs,
    local_file.clouds
  ]

}




###output "cluster_attrs_debug" {
###  value = local.clusters_attrs
###}



# Create databases for clusters
resource "vkcs_db_database" "databases" {
  
  for_each = local.databases

  name        = each.value.db_name
  dbms_id     = vkcs_db_cluster.clusters[each.value.dbms_name].id
  charset     = each.value.charset
  collate     = each.value.collate

  
  depends_on = [
    vkcs_db_cluster.clusters
  ]
}




###output "Cluster-id" {
###  value = vkcs_db_cluster.clusters["wp-db-vector"].id
###}




# Create users for databases
resource "vkcs_db_user" "db-users" {

  for_each = {for user in flatten([for cluster_key, cluster_value in var.clusters : [
            for user_name, user_value in cluster_value.users != null ?
              cluster_value.users :
                jsondecode(file("${var.DATA_DIR}/.TF_db_users.json")) : {
                  dbms_name = cluster_key
                  databases = user_value.databases
                  user_name = user_name
                  user_password = user_value.password
                  host = user_value.host
          }]]) : "${user.dbms_name}.${user.user_name}" => user}
             
  name        = each.value.user_name
  password    = each.value.user_password
  host        = each.value.host  
  dbms_id     = vkcs_db_cluster.clusters[each.value.dbms_name].id
  databases   = each.value.databases
  depends_on = [
    vkcs_db_database.databases
  ]
}


# Create external loadbalancers
resource "openstack_lb_loadbalancer_v2" "loadbalancers" {

  for_each = var.loadbalancers

  name          = each.key
 // vip_port_id = openstack_networking_port_v2.lb_ports[each.key].id
  vip_subnet_id = openstack_networking_subnet_v2.subnets["${each.value.network_port.net-name}.${each.value.network_port.subnet-name}"].id
  
  depends_on = [
    openstack_networking_subnet_v2.subnets
  ]
}


# Create listeners for loadbalancers
resource "openstack_lb_listener_v2" "listeners" {
  
  for_each = local.listeners

  name            = each.value.name
  protocol        = each.value.protocol
  protocol_port   = each.value.protocol_port
  default_tls_container_ref = each.value.default_tls_container_name == null ? null : openstack_keymanager_container_v1.sec_containers[each.value.default_tls_container_name].container_ref
  loadbalancer_id = openstack_lb_loadbalancer_v2.loadbalancers[each.value.lb_name].id
  insert_headers = each.value.insert_headers

  depends_on = [
    openstack_lb_loadbalancer_v2.loadbalancers,
    openstack_keymanager_container_v1.sec_containers
  ]
}


# Create pools for listeners
resource "openstack_lb_pool_v2" "lb-pools" {

  for_each = var.lb_pools

  protocol    = each.value.protocol
  lb_method   = each.value.lb_method

  dynamic "persistence" {
  
    for_each = each.value.persistence
    content {
        type        = persistence.value["type"]
        cookie_name = persistence.value["cookie_name"]
    }
  }

  listener_id = each.value.listener == null ? null : openstack_lb_listener_v2.listeners[each.value.listener].id
  
  depends_on = [
    openstack_lb_listener_v2.listeners
  ]
}


# Add members to pools
resource "openstack_lb_members_v2" "pools_members" {

  for_each = var.lb_pools

  pool_id = openstack_lb_pool_v2.lb-pools[each.key].id

  dynamic "member" {
    for_each = each.value.members
    content {
      name          = "${each.key}.${member.key}"
      address       = vkcs_compute_instance.instances[member.key].access_ip_v4 
      protocol_port = member.value.protocol_port
      weight        = member.value.weight
    }
  }
  
  depends_on = [
    openstack_networking_port_v2.ports,
    openstack_lb_pool_v2.lb-pools
  ]
}


# Create L7 policies for loadbalancers
resource "vkcs_lb_l7policy" "lb-l7-policies" {

  for_each = {for policy in flatten([for lb_key, lb_value in var.loadbalancers : [
                for policy_key, policy_value in lb_value.policies : {
                  lb_name          = lb_key   
                  name             = policy_key             
                  action           = policy_value.action          
                  description      = policy_value.description     
                  position         = policy_value.position        
                  listener_id      = openstack_lb_listener_v2.listeners["${lb_key}.${policy_value.listener}"].id 
                  redirect_pool_id = policy_value.redirect_pool == null ? "null" : openstack_lb_pool_v2.lb-pools[policy_value.redirect_pool].id
                  redirect_url     = policy_value.redirect_url == null ? "null" : policy_value.redirect_url
              }]]) : "${policy.lb_name}.${policy.name}" => policy}

    name             = each.value.name            
    action           = each.value.action          
    description      = each.value.description     
    position         = each.value.position        
    listener_id      = each.value.listener_id     
    redirect_pool_id = each.value.redirect_pool_id == "null" ? null : each.value.redirect_pool_id
    redirect_url     = each.value.redirect_url == "null" ? null : each.value.redirect_url

    depends_on = [
      openstack_lb_members_v2.pools_members
    ]
}


# Create redirections rules for loadbalancers
resource "vkcs_lb_l7rule" "lb-l7-rules" {

  for_each = {for rule in flatten([for lb_key, lb_value in var.loadbalancers : [
                for rule_key, rule_value in lb_value.rules : {
                  lb_name      = lb_key   
                  name         = rule_key             
                  l7policy_id  = vkcs_lb_l7policy.lb-l7-policies["${lb_key}.${rule_value.l7policy}"].id
                  compare_type = rule_value.compare_type
                  type         = rule_value.type        
                  value        = rule_value.value       

              }]]) : "${rule.lb_name}.${rule.name}" => rule}

    l7policy_id  = each.value.l7policy_id  
    compare_type = each.value.compare_type 
    type         = each.value.type         
    value        = each.value.value        

    depends_on = [
      vkcs_lb_l7policy.lb-l7-policies
    ]
}


# Provide external IPs for loadbalancers
resource "openstack_networking_floatingip_v2" "lb-ext-ip" {

  for_each = var.loadbalancers

  pool    = "ext-net"
 // port_id = openstack_lb_loadbalancer_v2.loadbalancers[each.key].vip_port_id

  lifecycle {
    prevent_destroy = false
  }
}

# Associate external IP with loadbalancers
resource "openstack_networking_floatingip_associate_v2" "lb-ext-ip" {
  
  for_each = var.loadbalancers
  
  floating_ip = openstack_networking_floatingip_v2.lb-ext-ip[each.key].address
  port_id = openstack_lb_loadbalancer_v2.loadbalancers[each.key].vip_port_id

  depends_on = [
    openstack_networking_floatingip_v2.lb-ext-ip,
    openstack_lb_loadbalancer_v2.loadbalancers
  ]
}



# Add secrets
resource "vkcs_keymanager_secret" "secrets" {
  
  for_each = var.secrets

  name                 = each.key
  payload              = (each.value.path != null ? file("${path.module}/${each.value.path}") :
                          each.value.secret_type == "certificate" ? file("${var.DATA_DIR}/fullchain.pem") :
                            each.value.secret_type == "private" ? file("${var.DATA_DIR}/privkey.pem") :
                              null)
  secret_type          = each.value.secret_type
  payload_content_type = each.value.payload_content_type
}



# Create containers from secrets
resource "openstack_keymanager_container_v1" "sec_containers" {
  
  for_each = var.sec_containers
  
  name = each.key
  type = each.value.type


  dynamic "secret_refs" {

    for_each = each.value.secrets
    content {
      name = secret_refs.value.name
      secret_ref = vkcs_keymanager_secret.secrets[secret_refs.value.secret_ref].secret_ref
    }
  }

  depends_on = [
    vkcs_keymanager_secret.secrets
  ]
}

# Create keypair for administrator's terminal
resource "openstack_compute_keypair_v2" "adm-keypair" {
  name = var.adm_key_name 
}



/*
# Add monitors to loadbalancers

resource "vkcs_lb_monitor" "lb_monitors" {
  
  for_each = local.listeners

  pool_id        = openstack_lb_pool_v2.lb-pools[each.key].id

  type           = each.value.pool.health_mon.type
  delay          = each.value.pool.health_mon.delay
  timeout        = each.value.pool.health_mon.timeout
  max_retries    = each.value.pool.health_mon.max_retries
  http_method    = each.value.pool.health_mon.http_method
  url_path       = each.value.pool.health_mon.url_path
  expected_codes = each.value.pool.health_mon.expected_codes
 
  depends_on = [
    openstack_lb_pool_v2.lb-pools
  ]
}
  */


  
