variable "openstack_conf" {
  type = object({
    auth_url         = string
    tenant_id        = string
    region           = string
    user_name        = string
    user_domain_name = string
    password         = string
    clouds_cfg_name  = string
  })
  sensitive = true
}

variable "DATA_DIR" {
  type = string
}

variable "adm_key_name" {
  type = string
}
variable "sec_groups" {
  type = map(object({
    description = string
    delete_default_rules = string
    rules = list(string)
  }))
}


variable "rules" {
  type = map(object({
      direction = string
      ethertype = string
      protocol = string
      port_min = number
      port_max = number
      remote_ip_prefix = string
      remote_group = string
    }))
}

variable "networks"{
  type = map(map(object({     
       // subnet-name = string
        router-name = string
        cidr = string
        start = string
        end = string
        gw_ip = string
        dhcp = string
      })))
}

variable "loadbalancers" {
  type = map(object({
    network_port = object({
      net-name = string
      subnet-name = string
      ip = string
      sec_groups = list(string)
      dns_name = string
    })
    rules = map(object({
      compare_type = string
      type         = string
      value        = string
      l7policy     = string
    }))
    policies = map(object({
      action        = string
      description   = string
      position      = number
      listener      = string
      redirect_url  = string
      redirect_pool = string
    }))
    listeners = map(object({
      protocol       = string
      protocol_port  = number
      default_tls_container_name = string
      insert_headers = object({
        X-Forwarded-For = bool
      })
    }))
  }))
}

variable "lb_pools" {
  type = map(object({
        listener    = string
        protocol    = string
        lb_method   = string
        persistence = map(object({
          type        = string
          cookie_name = string
        }))
        monitors = map(object({
          type           = string
          delay          = number
          timeout        = number
          max_retries    = number
          http_method    = string
          url_path       = string
          expected_codes = string
        }))
        members = map(object({
          protocol_port = number
          weight        = number
//          network_port  = string
        }))
      }))
}
variable "instances" {
  type = map(object({
    ip = string
    image = string
    vol_type = string
    vol_size = number
    flavor_name = string
    key_pair = string
    sec_groups = list(string)
    az = string
    delete = bool
    adm    = bool
    network_name = string
    cloud_init  = list(string)
###     cloud_init_main = string
###     cloud_init_temp = string
###     cloud_init_adm  = string
    ports = map(object({
      net-name = string
      subnet-name = string
      ip = string
      sec_groups = list(string)
      dns_name = string
      default_if = bool
      ext_ip = bool
    }))
  }))
  
}

variable "clusters" {
  type = map(object({
    type = string
    version = string
    cluster_size = number
    flavor_id = string
    volume_size = number
    volume_type = string
    network = string
    dns_name = string
    sec_groups = list(string)
    key_pair = string
    availability_zone = string
    users   = map(object({
      password  = string
      host      = string
      databases = set(string)
    }))
    db = map(object({
      charset = string
      collate = string
    }))
    capabilities = map(object({
      settings = object({
        listen_port = number
      })
    }))
    autoexpand = bool
    max_disk_size = number
  }))
}

variable "secrets" {
  type = map(object({
    path = string
    secret_type = string
    payload_content_type = string
  }))
}

variable "sec_containers" {
  type = map(object({
    type = string
    secrets = map(object({ 
      name = string
      secret_ref = string
    }))
  }))
}
variable  "file_provisioners" {
  type = map(object({
#      source      = string
      destination = string
      host        = string
      user        = string
      private_key = string
    }))
}
/*
variable "dns_zones" {
  type = map(object({
    name = string
    email = string
    description = string
    ttl = number
    type = string
    records = map(object({
      name = string
      description = string
      ttl = string
      type = string
      records = list(string)
    }))
  }))
}*/
