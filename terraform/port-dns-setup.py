import openstack
import pprint
import json
import base64

from sys import argv

# Initialize and turn on debug logging
#openstack.enable_logging(debug=True)

# Initialize connection
conn = openstack.connect(cloud=argv[1])

ports = json.loads(str(base64.b64decode(argv[2]))[2:-1])

try:
  # Set up DNS for each port
  for port in ports:
    conn.network.update_port(port["port_id"],dns_name=port["dns_name"])
  
  print(f"DNS names for network ports were successfully set up")
except:
  print("Something gone wrong :-(")
