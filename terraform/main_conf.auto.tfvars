adm_key_name = "adm-key"
networks = {
  adm-network = {
    adm-subnet = {
      router-name   = "adm-router"
      cidr          =  "10.0.0.0/26"
      start         =  "10.0.0.10"
      end           =  "10.0.0.62"
      gw_ip         =  null
      dhcp          = true
    }},
  mon-network = {
    mon-subnet = {
      router-name = "mon-router"
      cidr        = "10.0.1.0/26"
      start       = "10.0.1.10"
      end         = "10.0.1.62"
      gw_ip       = null
      dhcp        = true
    }},
  app-network = {
    app-subnet = {
      router-name   = "app-router"
      cidr          = "10.0.2.0/26"
      start         =  "10.0.2.10"
      end           = "10.0.2.62"
      gw_ip         = null
      dhcp          = true
    }},
  db-network = {
    db-subnet = {
      router-name: "db-router"
      cidr: "10.0.3.0/26"
      start: "10.0.3.10"
      end: "10.0.3.62"
      gw_ip: null
      dhcp: true
    }}
}
sec_groups = {
  adm-main = {
    description: "Main administaror terminal"
    delete_default_rules = true
    rules = [
      "any-ssh-in",
      "all-wan-out"
    ]},
  app-main = {
    description: "Main group for app"
    delete_default_rules = true
    rules = [
      "all-wan-out",
      "adm-ssh",
      "local-icmp",
      "http-wan-in",
      "https-wan-in",
      "from-mon-telegraf",
      "from-mon-cadvisor",
      "from-mon-node-exporter"
    ]},
  mon-main = {
    description: "Main group for mon"
    delete_default_rules = true
    rules = [
      "from-wan-to-mon-grafana",
      "from-wan-to-mon-prometheus",
      "all-wan-out",
      "adm-ssh",
      "local-icmp" 
    ]},
  mon-in = {
    description: "For mon ingress connections"
    delete_default_rules = true
    rules = [
      "from-mon-telegraf",
      "from-mon-cadvisor",
      "from-mon-node-exporter"
    ]},
  adm = {
    description: "Tag for adm aux if"
    delete_default_rules = true
    rules = [
      "all-local-out",
      "local-icmp"
    ]},
  mon = {
    description: "Tag for mon aux if"
    delete_default_rules = true
    rules = [
      "all-local-out",
      "local-icmp"
    ]},
  app = {
    description: "Tag for app aux if"
    delete_default_rules = true
    rules = [
      "all-local-out",
      "local-icmp"
    ]},
  database = { 
    description: "For access from mon to apps"
    delete_default_rules = true
    rules = [
      "from-mon-telegraf",
      "from-mon-cadvisor",
      "from-mon-node-exporter",
      "adm-ssh",
      "local-icmp"
    ]}
}

rules = {
  from-mon-telegraf = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 9125
    port_max         = 9125
    remote_ip_prefix = null
    remote_group     = "mon"
  },
  from-mon-cadvisor = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 8081
    port_max         = 8081
    remote_ip_prefix = null
    remote_group     = "mon"
  },
  from-mon-node-exporter = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 9100
    port_max         = 9101
    remote_ip_prefix = null
    remote_group     = "mon"
  },
  from-wan-to-mon-grafana = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 3000
    port_max         = 3000
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  from-wan-to-mon-prometheus = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 9090
    port_max         = 9090
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  from-wan-to-mon-alert-manager = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 9093
    port_max         = 9093
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  from-wan-to-mon-telegraf = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 9125
    port_max         = 9125
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  from-wan-to-mon-cadvisor = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 8081
    port_max         = 8081
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  from-wan-to-mon-node-exporter = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 9100
    port_max         = 9100
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  http-to-app = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 80
    port_max         = 80
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  https-to-app = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 443
    port_max         = 443
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  all-wan-out = {
    direction        = "egress"
    ethertype        = "IPv4"
    protocol         = null
    port_min         = null
    port_max         = null
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  adm-ssh = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 22
    port_max         = 22
    remote_ip_prefix = null
    remote_group     = "adm"
  },
  all-local-out = {
    direction        = "egress"
    ethertype        = "IPv4"
    protocol         = null
    port_min         = null
    port_max         = null
    remote_ip_prefix = "10.0.0.0/16"
    remote_group     = null
  },
  all-from-mon-in = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = null
    port_max         = null
    remote_ip_prefix = null
    remote_group     = "mon"
  },
  any-ssh-in = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 22
    port_max         = 22
    remote_ip_prefix = "0.0.0.0/0"
    remote_group     = null
  },
  local-icmp = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "icmp"
    port_min         = null
    port_max         = null
    remote_ip_prefix = "10.0.0.0/16"
    remote_group      = null
  },
  adm-icmp = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "icmp"
    port_min         = null
    port_max         = null
    remote_ip_prefix = null
    remote_group     = "adm"
  },
  http-wan-in = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 80
    port_max         = 80
    remote_ip_prefix = "0.0.0.0/0"
    remote_group      = null
  },
  https-wan-in = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 443
    port_max         = 443
    remote_ip_prefix = "0.0.0.0/0"
    remote_group      = null
  },
  app-to-db = {
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_min         = 3306
    port_max         = 3306
    remote_ip_prefix = null
    remote_group     = "app"
  }
}


#!!!!!!!!!!!!!!!!!!Change flavor_name => 1-2-20


# Uses image: Ubuntu-20.04.1-202008 | d853edd0-27b3-4385-a380-248ac8e40956
# Uses volume type: e7886624-2d95-4175-b14a-d256d6961bd6 | high-iops
instances = {
  adm-node1-ms1 = {
    ip                = null
    network_name      = "adm-network"
    image             = "d853edd0-27b3-4385-a380-248ac8e40956"
    vol_type          = "high-iops"
    vol_size          = 20
    flavor_name       = "Basic-1-1-10"
    key_pair          = "coffeeteam"
    sec_groups        = [] 
    az                = "MS1"
    cloud_init        = ["./cloud-init/adm-node1-main.yml",
                         "./cloud-init/instance-template.yml"]
    delete            = true 
    adm               = true
    ports = {
      adm-connection = {
        net-name    = "adm-network"
        subnet-name = "adm-subnet"
        ip          = null
        sec_groups  = ["adm-main", "adm"]
        dns_name    = "adm-node1-ms1"
        default_if  = true
        ext_ip      = true
      },
      mon-connection = {
        net-name    = "mon-network"
        subnet-name = "mon-subnet"
        ip          = null
        sec_groups: ["adm", "mon-in"]
        dns_name    = "adm-node1-ms1-mon"
        default_if  = false
        ext_ip      = false
      },
      app-connection = {
        net-name    = "app-network"
        subnet-name = "app-subnet"
        ip          = null
        sec_groups  = ["adm"]
        dns_name    = "adm-node1-ms1-app"
        default_if  = false
        ext_ip      = false
      },
      db-connection = {
        net-name    = "db-network"
        subnet-name = "db-subnet"
        ip          = null
        sec_groups  = ["adm"]
        dns_name    = "adm-node1-ms1-db"
        default_if  = false
        ext_ip      = false
      }}
  },
  mon-node1-ms1 = {
    ip                = null
    network_name      = "mon-network"
    image             = "d853edd0-27b3-4385-a380-248ac8e40956"
    vol_type          = "high-iops"
    vol_size          = 20
    flavor_name       = "Basic-1-1-10"
    key_pair          = "add-adm-key"  
    sec_groups        = []
    az                = "MS1"
    delete            = true
    adm               = false
    cloud_init        = ["./cloud-init/mon-node1-main.yml",
                        "./cloud-init/instance-template.yml"]
    ports = {
      mon-connection = {
        net-name    = "mon-network"
        subnet-name = "mon-subnet"
        ip          = null
        sec_groups:  ["mon-main", "mon"]
        dns_name    = "mon-node1-ms1" 
        default_if  = true
        ext_ip      = true
      },
      app-connection = {
        net-name    = "app-network"
        subnet-name = "app-subnet"
        ip          = null
        sec_groups  = ["mon"]
        dns_name    = "mon-node1-ms1-app"
        default_if  = false
        ext_ip      = false
      },
      db-connection = {
        net-name    = "db-network"
        subnet-name = "db-subnet"
        ip          = null
        sec_groups  = ["mon"]
        dns_name    = "mon-node1-ms1-db"
        default_if  = false
        ext_ip      = false
      }}
  },
  wp-app1-ms1 = {
    ip                = null
    network_name      = "app-network"
    image             = "d853edd0-27b3-4385-a380-248ac8e40956"
    vol_type          = "high-iops"
    vol_size          = 20
    flavor_name       = "Basic-1-1-10"
    key_pair          = "add-adm-key"
    sec_groups        = [] 
    az                = "MS1"
    cloud_init        = ["./cloud-init/app-main.yml",
                         "./cloud-init/instance-template.yml"]
    delete            = true 
    adm               = false
    ports = {
      app-connection = {
        net-name    = "app-network"
        subnet-name = "app-subnet"
        ip          = null
        sec_groups  = ["app-main", "app"]
        dns_name    = "wp-app1-ms1"
        default_if  = true
        ext_ip      = false
      },
      db-connection = {
        net-name    =  "db-network"
        subnet-name = "db-subnet"
        ip          = null
        sec_groups  = ["app"]
        dns_name    = "wp-app1-ms1-db"
        default_if  = false
        ext_ip      = false
    }}
  },
    wp-app2-ms1 = {
    ip                = null
    network_name      = "app-network"
    image             = "d853edd0-27b3-4385-a380-248ac8e40956"
    vol_type          = "high-iops"
    vol_size          = 20
    flavor_name       = "Basic-1-1-10"
    key_pair          = "add-adm-key"
    sec_groups        = []
    az                = "MS1"
    cloud_init        = ["./cloud-init/app-main.yml",
                         "./cloud-init/instance-template.yml"]
    delete            = true
    adm               = false
    ports = {

      app-connection = {
        net-name    = "app-network"
        subnet-name = "app-subnet"
        ip          = null
        sec_groups  = ["app-main", "app"]
        dns_name    = "wp-app2-ms1"
        default_if  = true
        ext_ip      = false
      },
      db-connection = {
        net-name    =  "db-network"
        subnet-name = "db-subnet"
        ip          = null
        sec_groups  = ["app"]
        dns_name    = "wp-app2-ms1-db"
        default_if  = false
        ext_ip      = false
    }}
  }
}


# flavor: bf714720-78da-4271-ab7d-0cf5e2613f14 | Standard-2-8-50
clusters = {
  wp-db-vector = {
    type              = "galera_mysql"
    version           = "8.0"
    cluster_size      = 3
    flavor_id         = "bf714720-78da-4271-ab7d-0cf5e2613f14"
    volume_size       = 20
    volume_type       = "high-iops"
    network           = "db-network"
    dns_name          = "wp-db-cluster"
    sec_groups        = ["database"]
    key_pair          = "add-adm-key"
    availability_zone = "MS1"
    users = null 
###            {
###      wp_user = {
###        password  = "SomeStrongPa$$w0rd" 
###        host      = null
###        databases = ["wp-db"]
###      }
###    }

    db = {
      wp-db = {
        charset = "utf8"
        collate = "utf8_general_ci"
        }  
    }  
    capabilities = {
      node_exporter = {
        settings = {
          "listen_port" = 9100
        }
      },
      mysqld_exporter = {
        settings = {
          "listen_port" = 9101
        }
      }
    }
    autoexpand        = true
    max_disk_size     = 1024
  }
}

loadbalancers = {
  lb-wp-ms1 = {
    network_port = {
        net-name    = "app-network"
        subnet-name = "app-subnet"
        ip          = null
        sec_groups  = []
        dns_name    = "vector-wp"
    }
    listeners = {
      https-ms1 = {
        protocol      = "TERMINATED_HTTPS"
        protocol_port = 443
        default_tls_container_name = "coffeeteam_online_tls"
        insert_headers = {
         X-Forwarded-For = null
        }
      }
      http-ms1 = {
        protocol      = "HTTP"
        protocol_port = 80
        default_tls_container_name = null
        insert_headers = {
         X-Forwarded-For = null
        }
      }
      grafana-https-ms1 = {
        protocol      = "TERMINATED_HTTPS"
        protocol_port = 3000
        default_tls_container_name = "coffeeteam_online_tls"
        insert_headers = {
         X-Forwarded-For = null
        }
      }
    }
    policies = {
/*
      app-http-to-https = {
        action        = "REDIRECT_TO_URL"
        description   = "Redirect app http to https"
        position      = 2
        listener      = "http-ms1"
        redirect_url  = "https://vrsk.tk"
        redirect_pool = null
      },
      app-https-to-app-pool = {
        action        = "REDIRECT_TO_POOL"
        description   = "Redirect app https to apps pool"
        position      = 1
        listener      = "https-ms1"
        redirect_url  = null
        redirect_pool = "app-ms1"
      },
      http-to-https-grafana = {
        action        = "REDIRECT_TO_URL"
        description   = "Redirect http to https Grafana"
        position      = 4
        listener      = "http-ms1"
        redirect_url  = "https://monitoring.vrsk.tk"
        redirect_pool = null
      },
      mon-https-to-mon-pool = {
        action        = "REDIRECT_TO_POOL"
        description   = "Redirect https://monitoring. to Grafana pool"
        position      = 3
        listener      = "https-ms1"
        redirect_url  = null
        redirect_pool = "mon-ms1"
      },
      all-unknown-http-to-root = {
        action        = "REDIRECT_TO_URL"
        description   = "Redirect all unknown http to root https"
        position      = 50
        listener      = "http-ms1"
        redirect_url  = "https://vrsk.tk"
        redirect_pool = null
      },
*/
      redirect-to-https = {
        action        = "REDIRECT_TO_URL"
        description   = "Redirect http to https"
        position      = 1
        listener      = "http-ms1"
        redirect_url  = "https://vrsk.tk"
        redirect_pool = null
      }
    } 
    rules = {
/*
      if-monitoring = {
        l7policy     = "mon-https-to-mon-pool"
        compare_type = "EQUAL_TO"
        type         = "HOST_NAME"
        value        = "monitoring.vrsk.tk"
      },
      if-app = {
        l7policy     = "app-https-to-app-pool"
        compare_type = "EQUAL_TO"
        type         = "HOST_NAME"
        value        = "vrsk.tk"
      },
      if-app-http = {
        l7policy     = "app-http-to-https"
        compare_type = "EQUAL_TO"
        type         = "HOST_NAME"
        value        = "vrsk.tk"
      },
*/
      if-http = {
        l7policy     = "redirect-to-https"
        compare_type = "STARTS_WITH"
        type         = "PATH"
        value        = "/"
      }
    }
  }
}

lb_pools = {
  app-ms1 = {
    listener  = "lb-wp-ms1.https-ms1"
    protocol  = "HTTP"
    lb_method = "ROUND_ROBIN"
    persistence = {
      http_cookie = {
        type        = "HTTP_COOKIE"
        cookie_name = null
      }
    }
    monitors = {  
      tcp = {
        type           = "TCP"
        delay          = 5
        timeout        = 4
        max_retries    = 3
        http_method    = null
        url_path       = null
        expected_codes = null 
      }
    }
    members = {
      wp-app1-ms1 = {
        protocol_port = 80
        weight        = 10
//        network_port  = "app-connection"
      },
      wp-app2-ms1 = {
        protocol_port = 80
        weight        = 10
//        network_port  = "app-connection"
      }
    }
  },
  mon-ms1 = {
    listener  = "lb-wp-ms1.grafana-https-ms1"
    protocol  = "HTTP"
    lb_method = "ROUND_ROBIN"
    persistence = {
      http_cookie = {
        type        = "HTTP_COOKIE"
        cookie_name = null
      }
    }
    monitors = {  
      tcp = {
        type           = "TCP"
        delay          = 5
        timeout        = 4
        max_retries    = 3
        http_method    = null
        url_path       = null
        expected_codes = null 
      }
    }
    members = {
      mon-node1-ms1 = {
        protocol_port = 3000
        weight        = 10
//        network_port  = "mon-connection"
      }
    }
  }
}

secrets = {
  coffeeteam_online_cert = {
    path = null ###"../../vrsk.tk/fullchain.pem"
    secret_type = "certificate"
    payload_content_type = "text/plain"
  },
  coffeeteam_online_key = { 
    path = null ###"../../vrsk.tk/privkey.pem"
    secret_type = "private"
    payload_content_type = "text/plain"
  }
}

sec_containers = {
  coffeeteam_online_tls = {
    type = "certificate"
    secrets = {
      coffeeteam_online_cert = {
        name = "certificate"
        secret_ref = "coffeeteam_online_cert"
      },
      coffeeteam_online_key = {
        name = "private_key"
        secret_ref = "coffeeteam_online_key"
      }}
}}

file_provisioners = {
  add_id_rsa = {
#    source      = "tmp.id_rsa"
    destination = "/home/ubuntu/.ssh/id_rsa"
    host        = "adm-node1-ms1"
    user        = "ubuntu"
    # priv_key for connection
    private_key = null ###"../../adm_keypair/id_rsa"
  }
}

/* 
    ansible = {
      master      = true
//      run_ansible = true
      playbooks   = {
        init = {
          playbook_dir  = "/home/ansible/my-repo|
          playbook_name = "start-lxd.yml"
          timeout: 120  = ""
          forks         = 1
          private_key   = "/home/ansible/.ssh/id_rsa"
        }
      }
    }*/
