import openstack
import pprint
import json
import base64

from sys import argv

# Initialize and turn on debug logging
#openstack.enable_logging(debug=True)

# Initialize connection
# conn = openstack.connect(cloud='mordred')
conn = openstack.connect(cloud=argv[1])
  
### pp = pprint.PrettyPrinter(indent=2, compact=False)
#pp.pprint(argv[2])
###pp.pprint([*conn.compute.servers()])
###clusters = json.loads("[{\"cluster_id\":\"2a9516a9-31d4-46d1-ab1e-6267abe3004e\",\"cluster_name\":\"wp-db-vector\",\"dns_name\":\"wp-db-cluster\",\"loadbalancer\":{\"id\":\"\",\"vip_port_id\":\"\"},\"network\":{\"id\":\"860ffcfd-5f30-4cf1-8ed1-f663a9448c83\",\"name\":\"db-network\"},\"pool_instances\":[],\"security_groups\":[{\"id\":\"e7b4f5ba-e8ac-40c3-a0c8-2945fafafd0d\",\"name\":\"database\"}]}]")

###clusters = [ {
###              "cluster_id": "2a9516a9-31d4-46d1-ab1e-6267abe3004e",
###              "cluster_name": "wp-db-vector",
###              "dns_name"  : "wp-db-vector",
###              "network": {"name": "db-network", 
###                          "id": "860ffcfd-5f30-4cf1-8ed1-f663a9448c83"},
###              "loadbalancer": {"id": "", "vip_port_id": ""},
###              "pool_instances": [],
###              "security_groups": [{"id": "e7b4f5ba-e8ac-40c3-a0c8-2945fafafd0d",
###                                   "name": "database"}]
###              }
###           ]
###
###print(str(base64.b64decode(argv[2]))[2:-1])
###pp.pprint(json.loads(str(base64.b64decode(argv[2]))[2:-1]))

clusters = json.loads(str(base64.b64decode(argv[2]))[2:-1])

ip_set = set() # Set of cluster's instences's IP
all_rule_set = set() # Set of all existing rules on cluster instance security groups
rule_set = set() # Set of capabilities rules to delete

try:
  for cluster in clusters:
    # Try to find cluster's members's network ports
    for server in conn.compute.servers(network_id=cluster["network"]["id"]):
      if "cluster_id" in server.metadata.keys() \
          and server.metadata["cluster_id"] == cluster["cluster_id"]:

          # Get cluster's members attributes
          ip_set.add(server["addresses"][cluster["network"]["name"]][0]["addr"])

          cluster["pool_instances"].\
              append( {"name": server["name"], "id": server["id"],\
                "mac-addr": server["addresses"][cluster["network"]["name"]][0]["OS-EXT-IPS-MAC:mac_addr"],\
                "ip-addr": server["addresses"][cluster["network"]["name"]][0]["addr"],\
                "port-id": list(conn.network.ports(network_id=cluster["network"]["id"],\

              mac_address=server["addresses"][cluster["network"]["name"]][0]["OS-EXT-IPS-MAC:mac_addr"]))[0]["id"]})

          # Set cluster's members security groups
          for sec_group in cluster["security_groups"]:
            conn.compute.add_security_group_to_server(server["id"], sec_group)

    # Get cluster security groups
    for sec_group in conn.compute.get_server(cluster["pool_instances"][0]["id"])["security_groups"]:
     all_rule_set.add(sec_group["id"])

    # Find and delete default rules for cluster capabilities
    for sec_group_id in all_rule_set:
      # try remote_group_id=null?
      rule_set.update(rule["id"] for rule in conn.network.security_group_rules(security_group_id=sec_group_id,\
          remote_ip_prefix="0.0.0.0/0", direction="ingress"))
    for rule in rule_set:
      conn.network.delete_security_group_rule(rule)
    # Try to find cluster's loadbalancer's network port
    break_f = False
    for pool in conn.load_balancer.pools():
      for p_member in conn.load_balancer.members(pool):
        if p_member["address"] in ip_set:
          # Saving loadbalancers attribute
          tmp_lb = conn.load_balancer.get_load_balancer(pool["loadbalancers"][0]["id"])
          cluster["loadbalancer"]["id"] = tmp_lb["id"]
          cluster["loadbalancer"]["vip_port_id"] = tmp_lb["vip_port_id"]

          # Oh,we found cluster's loadbalancer port, so let's set up DNS name for it.
          conn.network.update_port(tmp_lb["vip_port_id"],dns_name=cluster["dns_name"])
          break_f = True
          break
      if break_f:
        print(f"DNS name and security groups for [{cluster['cluster_name']}] were successfully set up")
        break
except:
  print("Something gone wrong :-(")


###pp.pprint(clusters)

